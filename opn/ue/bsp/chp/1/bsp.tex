\begin{question}[section=1,subtitle={Canny Edge Detector},difficulty=,mode=ue,type=bsp,tags={}]
	The goal of this exercise is to familiarise yourself with edge detection.
	You will implement the Canny edge detector, one of the most common edge detectors.
	A code construct and an example image is provided in a zip file which are in the repo.


Use these files as templates for your program and refer to the comments in the files for details on expected inputs and outputs.
\begin{compactenum}[I.]
\item  \textbf{Implementation:}
You should write code so that it can be understood by someone else (the tutors, for example).
Comment sections and lines which might not be easy to understand.
If you've thought about a line or section for more than a couple of minutes, it probably warrants a comment.
Use descriptive variable names.
Your code should not take too long to run.
For the first few exercises, anything more than a few seconds likely means you are doing something very inefficient.
Points may be deducted for slow code, even if your solution is correct.
Complete the function skeletons provided, implementing edge detection on a grayscale image using the Canny edge detector.

The algorithm consists of 4 main parts:
\begin{compactenum}[1.]
\item \textbf{Image blurring:} \addpoints{1}
Blur the image with a Gauss-filter.
Use the \Matlab commands \texttt{fspecial} and \texttt{imfilter} (with option \texttt{replicate}).
The width of the square filter kernel should be calculated as follows:
\begin{equation}
\text{kernel}_{\text{width}} = 2  \text{round}(3  \text{smooth}_\sigma) + 1
\end{equation}
Complete the code in \texttt{blur\_gauss.m}.
\item \textbf{Edge detection:} \addpoints{2} 
Calculate the horizontal and vertical gradient of the image using a Sobel filter, transposing it as appropriate.
Again, use the \Matlab commands \texttt{fspecial} and \texttt{imfilter}.
You may find \texttt{circle.jpg} useful for testing whether your gradient and orientation results are what you expect.
Complete the code in \texttt{sobel.m}.
\item \textbf{Non-maximum suppression:} \addpoints{4}
Implement the non-maximum suppression algorithm along the orientation of the gradients to thin out the edges.
You may wish to quantise the gradient orientations to discrete levels like "horizontal", "vertical", "diagonal-left", "diagonal-right" first and then treat each case separately.
\eg if a pixel has a "horizontal" gradient orientation, its gradient value should only be kept if its immediate left and right neighbor pixels have a lower gradient value than the center pixel. 
Complete the code in \texttt{non\_max.m}.
Make sure you explain your approach in comments.
\item \textbf{Hysteresis thresholding:} \addpoints{1}
Implement hysteresis thresholding to receive connected edge segments.
The \Matlab commands \texttt{find} and \texttt{bwselect} will reduce your programming effort.
To make parametrisation easier you should normalize the gradient values to lie within the interval $[0, 1]$ before applying the hysteresis thresholding.
Complete the code in \texttt{hyst\_thresh.m}.
\item \textbf{Automatic hysteresis threshold selection:} \addpoints{2}
The hysteresis thresholds can be difficult to select manually, as you will probably discover if you run the code on various images.
It's not clear from looking at an image what thresholds you should use.
One useful tool for analysing image content (and other data) is the histogram, which is essentially a bar graph where each bar represents how many values in the input fall into a certain range.
You can generate a histogram with the \texttt{histogram} or \texttt{histcounts} commands in \Matlab.

Using the histogram, you should implement a simple automatic method for selecting the hysteresis thresholds.
We will assume that some proportion of edge pixels should be above the high threshold, and some proportion above the low threshold.
The \texttt{cumsum} function will be useful.
Your computation should be precise - don't just use the bin centre or edge for the thresholds.
Assume that values which fall in one bin are uniformly distributed within it: a bin with \num{100} points in it will have \num{20} points in the first fifth of its range, \num{40} within the first two fifths, and so on.

You should use a \num{50}-bin histogram for a reasonable degree of granularity.
You should also exclude all zero values, since they are of no interest to us.

Complete the code in \texttt{hyst\_thresh\_auto.m}.
\end{compactenum}
Forbidden and allowed \Matlab commands:

Matlab command \texttt{edge} must not be used.
However, the commands \texttt{imfilter}, \texttt{conv} and \texttt{conv2}, along with other block-processing and sliding-window functions may be used, if needed.
Unless explicitly stated, you should not use functions which solve an exercise, or a significant part of it, in a single line of code.
If you are unsure of whether you are allowed to use a function, please contact the tutors.

Since \Matlab is a matrix-oriented programming language, "vectorised" processing is much more efficient than loops.
Pictures should be converted to floating point format before further processing; convert your picture to a double grayscale image (function \texttt{rgb2gray}).
Also make sure that your starting image only contains RGB values in the \texttt{uint8} range from \num{0} to \num{255}.
Keep your programs as simple as possible.
Catching wrong user inputs and printing error messages is not necessary.

If your code is slow, the "Run and Time" button in the \Matlab editor tab allows you to profile the code and see which lines are the most time consuming.

\textbf{Useful commands:}
\begin{compactitem}
\item \texttt{imfilter} - Filtering with linear filter kernel
\item \texttt{fspecial} - Generation of filter kernels
\item \texttt{imread} - Load an image
\item \texttt{imshow} - Display an image (for RGB image, value format is \texttt{uint8} with range $[0, 255]$, for grayscale, value format is double with range $[0, 1]$)
\item \texttt{rgb2gray} - Image conversion to grayscale
\item \texttt{double} - Converts to double-precision floating point format
\item \texttt{uint8} - Converts to 8-bit unsigned integer format
\item \texttt{imresize} - Resizes an image
\item \texttt{edge} - Edge detection
\item \texttt{bwselect} - Selects objects in binary images
\item \texttt{histogram} - Histogram plot
\item \texttt{max} - Finds maximum
\end{compactitem}

\item \textbf{Documentation:}
Perform the following experiments and answer the questions in a few sentences.
Only change the parameters in the file \texttt{main\_ex1.m} in the marked places.
The \texttt{imrow} function provided in the exercise files may be useful for supporting your answers.
\begin{compactitem}
    \item Answer each numbered question separately, with the answer labelled according to the experiment and question number.
    For example, experiment 1 question 2 should be numbered 1.2.
    You do not need to include the question text.
    \item Include images to support your answers.
\end{compactitem}
Maximum \num{1500} words, arbitrary number of pages for result images (referenced in the text).
Violating the text limit will lead to point deductions!
\begin{compactenum}[1.]
\item \textbf{Investigate the effect of \texttt{sigma}:} \addpoints{2} 
\begin{compactenum}
 \item Why is the kernel width defined as a function of sigma?
    Try various width and sigma inputs for the \texttt{fspecial} function and examine the results.
    Does an incorrect width have an effect in practice?
    \item What effect does blurring have on pixel intensities in the image?
    Use \texttt{imrow} here.
    \item What is its effect on the edges you receive after non-max suppression?
   \item How does it affect the edges you are left with after the hysteresis thresholding, assuming thresholds are kept constant?
    \end{compactenum}
\item \textbf{Investigate the effect of thresholds used during hysteresis thresholding:} \addpoints{2}
\begin{compactenum}
    \item Change the parameters \texttt{thresh\_low} and \texttt{thresh\_high}.
    What effects do these values have on the result?
    \item You might notice that keeping thresholds the same over different images can give inconsistent results (try it out).
    Why is this?
    Can you think of situations in which you might wish to keep the thresholds the same?
    \item Does the automatic selection help with getting reasonable edges across various images?
    Why?
    \item Is there a situation where the selection method you implemented doesn't work well?
    How might one mitigate this issue?
\end{compactenum}
\item \textbf{Noise:} \addpoints{1}
Investigate the effect of noise on the results.
You can add noise to an image with the function \texttt{imnoise}.
Add gaussian noise with zero mean and try variances in the range $[\num{0.001}, \num{0.25}]$.
\begin{compactenum}
    \item What effect does the noise have on the resulting edges?
    \item Is it possible to set the parameters to get reasonable edges?
    How does changing \texttt{sigma} and the hysteresis threshold values affect the results?
\end{compactenum}
\item \textbf{Marr-Hildreth:} \addpoints{1}
The Marr-Hildreth Operator, also called Laplacian of Gaussian Operator, uses the second derivative of the gradients instead of the first to localize the edges.
Compare this operator - \Matlab function \texttt{edge(img, 'log')} - with your implementation of the Canny algorithm.
What differences can you see?
\end{compactenum}
\end{compactenum}
\end{question}
\begin{solution}
	
\end{solution}



